const express = require("express");
const { TaskList, CreateTask , SingleTask , UpdateTask} = require("../middlewares/Task.middleware");
const TaskModel = require("../models/Task.model");
const router = express.Router();

router.get("/list", TaskList);
router.post("/create", CreateTask);
router.get("/:id", SingleTask);
router.put("/update_task/:id", UpdateTask);


module.exports = router;
