const express = require("express");
const { TodoList, CreateTodo , SingleTodo , UpdateTodo } = require("../middlewares/Todo.middleware");
const TodoModel = require("../models/Todo.model");
const router = express.Router();

router.get("/list", TodoList);
router.post("/create", CreateTodo);
router.get("/:id", SingleTodo);
router.put("/update_todo/:id", UpdateTodo);
  

module.exports = router;
