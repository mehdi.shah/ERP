const express = require("express");
const {
  ProjectList,
  CreateProject,
  SingleProject,
  UpdateProject,
} = require("../middlewares/Project.middleware");
const ProjectModel = require("../models/Project.model");

const router = express.Router();
router.get("/list", ProjectList);
router.post("/create", CreateProject);
router.get("/:id", SingleProject);
router.put("update_project/:id", UpdateProject);

module.exports = router;
