const express = require("express");
const { UserList, CreateUser , SingleUser , UpdateUser} = require("../middlewares/User.middleware");
const Model = require("../models/User.model");
const router = express.Router();
const JWT = require("jsonwebtoken");


router.get("/list", UserList);
router.post("/create", CreateUser);
router.get("/:id", SingleUser);
router.put("/update_user/:id", UpdateUser);


module.exports = router;
