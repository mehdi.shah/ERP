const express = require("express");
const { Eventlist, CreateEvent , SingleEvent , UpdateEvent} = require("../middlewares/Event.middleware");
const EventModel = require("../models/Event.model");
const router = express.Router();

router.get("/list", Eventlist);
router.post("/create", CreateEvent);
router.get("/:id", SingleEvent);
router.put("/update_event/:id", UpdateEvent);
  

module.exports = router;


