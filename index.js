const express = require("express");
const bodyparser = require("body-parser");
const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");


const app = express();
const PORT = 8000;

const nodemailer = require('nodemailer');
const UserModel = require('./middlewares/User.middleware');

const UserRouter = require("./routes/User.route");
const ProjectRouter = require("./routes/Project.route");
const TaskRouter = require("./routes/Task.route");
const TodoRouter = require("./routes/Todo.route");
const EventRouter = require("./routes/Event.route");

//DB config
require("./config/db");

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

//"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYXRpb25hbF9pZCI6IjIzODU5OTg1IiwicGhvbmVfbnVtYmVyIjoiOTE5Mzg1NTU5MSIsImlhdCI6MTYyNTA2MjkyMX0.SLvSCmV7wDV9io4I3tw4rtGnhmdfWSRicP5-puyQK3o",
/*
"first_name":"mehdi",
    "last_name":"shahsavari",
    "address":"Tehran",
    "national_id":"23795451",
    "phone_number":"9197672179",
    "password":"mhdy132357",
    "is_valid":"true",
    "email":"mehdi.shahsavari.132357@gmail.com"
*/

app.use(cors());
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.use("/api/v2/user", UserRouter);
app.use("/api/v2/project", ProjectRouter);
app.use("/api/v2/task", TaskRouter);
app.use("/api/v2/todo", TodoRouter);
app.use("/api/v2/event", EventRouter);




//send email https://myaccount.google.com/lesssecureapps?pli=1
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'mahdi.shahsavari79@gmail.com',
    pass: 'mhdy132357'
  }
});
  
var mailOptions = {
  from: 'mahdi.shahsavari79@gmail.com',
  to: 'khar.shahsavari.132357@gmail.com',
  subject: 'Sending Email using Node.js',
  html: '<h1>This is from Settia company</h1><p>That was easy mhdy!</p>',
  attachments : [{
    filename : "Factormhdy.txt",
    path : "/home/eesssn/Desktop/Factor.txt"
  }]
};
  
transporter.sendMail(mailOptions, function(error, info){
  if (error) {
    console.log(error);
  } else {
    console.log('Email sent: ' + info.response);
  }
});



