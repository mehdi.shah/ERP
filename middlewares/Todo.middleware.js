const TodoModel = require("../models/Todo.model");
const UserModel = require("../models/User.model");
const JWT = require("jsonwebtoken");
const TodoList = (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  UserModel.findOne({national_id: decoded.national_id}).then((user) =>{
    TodoModel.create({created_by:user._id})
   })
   .catch((err) => {
    console.log(err);
    res.status(500).json({message:"error"})
   })
  TodoModel.find({
    isDeleted: false,
   })
    .then((result) => {
      res.status(200).json({ success: true, data: result });
      console.log("everything is ok");
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ message: "server is down" });
    });
};



const SingleTodo = async (req, res , next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await UserModel.findOne({national_id:decoded.national_id})
  try {
    // Check if todo exist or not
    let todo = await TodoModel.findById(req.params.id);
    if (!todo) {
      return res.json({
        success: false,
        message: "Todo ID doesn't exist",
      });
    } else {
      res.json({
        success: true,
        message: "Todo found successfully",
        todo: todo,
      });
    }
  } catch (error) {
    next(error);
  }
};



const UpdateTodo = async (req, res, next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await UserModel.findOne({national_id:decoded.national_id})
  try {
    // Check if todo exist or not
    let todo = await TodoModel.findById(req.params.id);

    if (!todo) {
      return res.json({
        success: false,
        message: "Todo ID doesn't exist",
      });
    } else {
      let updateTodo = await TodoModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        {
          new: true,
          runVaidator: true,
        }
      );

      res.json({
        success: false,
        message: "Todo updated successfully.",
        todo: updateTodo,
      });
    }
  } catch (error) {
    next(error);
  }
};



const CreateTodo = async (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  const creator = await UserModel.findOne({national_id:decoded.national_id })
  console.log("created_by:", decoded.national_id)
  
  const todo = {
    title: data.title,
    assign_to: data.assign_to,
    date: new Date(),
    priority: null,
    task_id: data.task_id,
    created_by: creator._id,
    created_at: new Date(),
    is_deleted: false,
    deleted_by: null,
    deleted_at: null,
    updated_by: null,
    updated_at: null,
  };
  new TodoModel(todo)
    .save()
    .then((result) => {
      res.status(201).json({ is_success: true, data: result });
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: err, is_success: false });
    });
  
};


module.exports = { TodoList , CreateTodo , UpdateTodo , SingleTodo};