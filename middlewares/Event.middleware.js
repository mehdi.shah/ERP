const EventModel = require("../models/Event.model");
const UserModel = require("../models/User.model");
const JWT = require("jsonwebtoken");


const Eventlist = async(req , res) => {
    const data = req.body
    const decoded = JWT.decode(data.token);
    await UserModel.findOne({national_id:decoded.national_id}).then((user) => {
        EventModel.find({created_by:user._id})
    })
    EventModel.find({
        is_deleted: false,
    }).then((resault) => {
        res.status(200).json({ success: true, data: resault });
        console.log("ok!");
    }).catch((err) => {
        console.log(err);
        res.status(500).json({ message: "droped!"});
    });
};



const SingleEvent = async (req, res , next) => {
    const data = req.body
    const decoded = JWT.decode(data.token);
    await UserModel.findOne({national_id:decoded.national_id})
    try {
      // Check if task exist or not
      let event = await EventModel.findById(req.params.id);
      if (!event) {
        return res.json({
          success: false,
          message: "Event ID doesn't exist",
        });
      } else {
        res.json({
          success: true,
          message: "Event found successfully",
          event: event,
        });
      }
    } catch (error) {
      next(error);
    }
};





const UpdateEvent = async (req, res , next) => {
    const data = req.body
    const decoded = JWT.decode(data.token);
    await UserModel.findOne({national_id:decoded.national_id})
    try {
      // Check if event exist or not
      let event = await EventModel.findById(req.params.id);
  
      if (!event) {
        return res.json({
          success: false,
          message: "Event ID doesn't exist",
        });
      } else {
        let updateEvent = await EventModel.findByIdAndUpdate(
          req.params.id,
          req.body,
          {
            new: true,
            runVaidator: true,
          }
        );
  
        res.json({
          success: false,
          message: "Event updated successfully.",
          event: updateEvent,
        });
      }
    } catch (error) {
      next(error);
    }
};
  
  
  
  
const CreateEvent = async (req, res) => {
    const data = req.body
    const decoded = JWT.decode(data.token);
    const creator = await UserModel.findOne({national_id:decoded.national_id })
    console.log("created_by:", decoded.national_id)
    
    const event = {
        name: data.name,
        members: data.members,
        date: new Date(),
        time: Date.now,
        created_by: creator._id,
        created_at: new Date(),
        is_deleted: false,
        deleted_by: null,
        deleted_at: null,
        updated_by: null,
        updated_at: null,
    };
    new EventModel(event)
    .save()
    .then((result) => {
      res.status(201).json({ is_success: true, data: result });
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: err, is_success: false });
    });

};

module.exports = { Eventlist, CreateEvent, SingleEvent , UpdateEvent }