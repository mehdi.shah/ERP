const ProjectModel = require("../models/Project.model");
const UserModel = require("../models/User.model");
const JWT = require("jsonwebtoken");

const ProjectList = (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  UserModel.findOne({national_id:decoded.national_id}).then((user) => {
    ProjectModel.find({manager:user._id})
  //   .then((projects) => {
  //     let options = ["Pending","In_Progress","Done","Canceled"];
  //     let result = [];
  //     const flag = false;
  //     // length is used as the numbers of projects parameter 
  //     for (let i = 0; i < projects.length; i++) {
  //       // x replace with the options parameter like x = Pending
  //       let arr = options.filter(x => x !== projects[i].status)
  //       let obj = { 
  //         ...projects[i],
  //         availableStatus:arr
  //       }
  //       result.push(obj);
  //       if(i === projects.length) flag = true;
  //     }
  //     if(flag) res.status(200).json({ success: true, data: result});
  //   })
  //   .catch(err => {
  //     console.log(err);
  //     res.status(500).json({ message: "error"})
  //   })
  // }).catch((err) => {
  //   console.log(err);
  //   res.status(500).json({ message: "error"})
    })
  ProjectModel.find({
    is_deleted: false,
  })
    .then((result) => {
      res.status(200).json({ success: true, data: result });
      console.log("project status is ok!");
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ message: "server is down" });
    });
};


const SingleProject = async (req, res, next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await UserModel.findOne({national_id:decoded.national_id})
  try {
    // Check if project exist or not
    let project = await ProjectModel.findById(req.params.id);
    if (!project) {
      return res.json({
        success: false,
        message: "User ID doesn't exist",
      });
    } else {
      res.json({
        success: true,
        message: "Project found successfully",
        project: project,
      });
    }
  } catch (error) {
    next(error);
  }
};
  

const UpdateProject = async (req, res , next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await UserModel.findOne({national_id:decoded.national_id})
  try {
    // Check if project exist or not
    let project = await ProjectModel.findById(req.params.id);

    if (!project) {
      return res.json({
        success: false,
        message: "Project ID doesn't exist",
      });
    } else {
      let updateProject = await ProjectModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        {
          new: true,
          runVaidator: true,
        }
      );

      res.json({
        success: false,
        message: "Project updated successfully.",
        project: updateProject,
      });
    }
  } catch (error) {
    next(error);
  }
};



const CreateProject = async (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  const creator = await UserModel.findOne({national_id:decoded.national_id })
  console.log("created_by:", decoded.national_id)
  
  const project = {
    about_project: data.about_project ,
    start_date: new Date() ,
    end_date: new Date(),
    owner: data.owner ,
    tags: data.tags,
    budget: data.budget,
    manager: data.manager,
    members: data.members,
    created_by: creator._id,
    created_at: new Date(),
    is_deleted: false,
    deleted_by: null,
    deleted_at: null,
    updated_by: null,
    updated_at: null,
    status: "Pending",
  };
  new ProjectModel(project)
    .save()
    .then((result) => {
      res.status(201).json({ is_success: true, data: result });
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: err, is_success: false });
    });
};
  
module.exports = { ProjectList, CreateProject , SingleProject , UpdateProject};
