const TaskModel = require("../models/Task.model");
const ProjectModel = require("../models/Project.model");
const UserModel = require("../models/User.model");
const JWT = require("jsonwebtoken");
const Pusher = require("pusher");



const TaskList = (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  UserModel.findOne({national_id:decoded.national_id}).then((user) => {
    TaskModel.find({created_by:user._id})
  //   .then((tasks) => {
  //     let options = ["ToDo","In_Progress","Review","Done"];
  //     let priorities = ["High" , "Medium" , "Low"];
  //     let resultoptions = [];
  //     let resultpriorities = [];
  //     const sts = false;
  //     for (let i = 0; i < tasks.length; i++) {
  //       let arr = options.filter(x => x !== tasks[i].status);
  //       let list = priorities.filter(x => x !== tasks[i].priority);
  //       let obj = {
  //         ...tasks[i],
  //         availableStatus:arr
  //       }
  //       let thing = {
  //         ...tasks[i],
  //         availablePriorities:list
  //       }
  //       resultoptions.push(obj);
  //       resultpriorities.push(thing);
  //       if(i === tasks.length) sts = true;
  //     }
  //     if(sts) res.status(200).json({ success: true, data: {resultoptions , resultpriorities}});
  //   })
  //   .catch(err => {
  //     console.log(err);
  //     res.status(500).json({ message: "error"})
  //   })
  // }).catch((err) => {
  //   console.log(err);
  //   res.status(500).json({ message: "error"})
    })
  TaskModel.find({
    is_deleted: false,
  })
    .then((result) => {
      res.status(200).json({ success: true, data: result });
      console.log("everything is ok");
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ message: "server is down" });
    });
};


const SingleTask = async (req, res , next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await UserModel.findOne({national_id:decoded.national_id})
  try {
    // Check if task exist or not
    let task = await TaskModel.findById(req.params.id);
    if (!task) {
      return res.json({
        success: false,
        message: "Task ID doesn't exist",
      });
    } else {
      res.json({
        success: true,
        message: "Task found successfully",
        task: task,
      });
    }
  } catch (error) {
    next(error);
  }
};


const UpdateTask = async (req, res , next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await UserModel.findOne({national_id:decoded.national_id})
  try {
    // Check if task exist or not
    let task = await TaskModel.findById(req.params.id);

    if (!task) {
      return res.json({
        success: false,
        message: "Task ID doesn't exist",
      });
    } else {
      let updateTask = await TaskModel.findByIdAndUpdate(
        req.params.id,
        req.body,
        {
          new: true,
          runVaidator: true,
        }
      );

      res.json({
        success: false,
        message: "Task updated successfully.",
        task: updateTask,
      });
    }
  } catch (error) {
    next(error);
  }
};



const pusher = new Pusher({
  appId: "1233938",
  key: "3d0beda052ebc91049bf",
  secret: "74b06001447886feb16c",
  cluster: "mt1",
  useTLS: true
});



const CreateTask = async (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  const creator = await UserModel.findOne({national_id:decoded.national_id })
  console.log("created_by:", decoded.national_id)

  pusher.trigger("my-channel", "my-event", {
    message: "hello world this is Settia Company"
  });
  
  const task = {
    title: data.title,
    assign_to_user: data.assign_to_user,
    assign_to_unit: data.assign_to_unit,
    date: new Date(),
    due_date: data.due_date,
    short_description: data.short_description,
    description: data.description,
    created_by: creator._id,
    created_at: new Date(),
    is_deleted: false,
    deleted_by: null,
    deleted_at: null,
    updated_by: null,
    updated_at: null,
    status: data.status,
    priority: data.priority,
    project_id: null,
  };
  new TaskModel(task)
    .save()
    .then((result) => {
      res.status(201).json({ is_success: true, data: result });
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: err, is_success: false });
    });
  
};

module.exports = { TaskList, CreateTask, SingleTask , UpdateTask };
