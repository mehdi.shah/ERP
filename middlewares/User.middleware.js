const Model = require("../models/User.model");
const bcrypt = require("bcryptjs");
const JWT = require("jsonwebtoken");


const UserList = (req, res) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  Model.findOne({national_id:decoded.national_id})

  Model.find({
    is_deleted: false,
  })
    .then((result) => {
      res.
      status(200).
      json({ success: true, data: result });
      console.log("everything is ok");
    })
    .catch((err) => {
      console.log(err);
      res.status(500).
      json({ message: "server is down" });
    });
};


const SingleUser = async (req, res , next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await Model.findOne({national_id:decoded.national_id})
  try {
    // Check if user exist or not
    let user = await Model.findById(req.params.id);
    if (!user) {
      return res.json({
        success: false,
        message: "User ID doesn't exist",
      });
    } else {
      res.json({
        success: true,
        message: "User found successfully",
        user: user,
      });
    }
  } catch (error) {
    next(error);
  }
};


const UpdateUser = async (req, res, next) => {
  const data = req.body
  const decoded = JWT.decode(data.token);
  await Model.findOne({national_id:decoded.national_id})
  try {
    // Check if user exist or not
    let user = await Model.findById(req.params.id);

    if (!user) {
      return res.json({
        success: false,
        message: "User ID doesn't exist",
      });
    } else {
      let updateUser = await Model.findByIdAndUpdate(
        req.params.id,
        req.body,
        {
          new: true,
          runVaidator: true,
        }
      );

      res.json({
        success: false,
        message: "User updated successfully.",
        user: updateUser,
      });
    }
  } catch (error) {
    next(error);
  }
};



const CreateUser = async (req, res) => {
  const data = req.body;
  const private_key = 'mhdy132357@MHDY132357';
  const token = JWT
  .sign({ national_id:data.national_id ,
        phone_number:data.phone_number }
       ,private_key);
  const existing_national_id = await Model.findOne({
    national_id: data.national_id,
  });
  if (existing_national_id)
    return res
      .status(400)
      .json({ is_success: false, message: "national id existed!" });
  const existing_phone_number = await Model.findOne({
    phone_number: data.phone_number,
  });
  if (existing_phone_number)
    return res
      .status(400)
      .json({ is_success: false, message: "phone numebr existed!" });
  const existing_email = await Model.findOne({
    email:data.email,
  });
  if (existing_email)
    return res
      .status(400)
      .json({ is_success: false , message: "email address existed."});

  let salt = bcrypt.genSaltSync(12);
  let hash_password = bcrypt.hashSync(data.password, salt);
  console.log("password difference:", data.password, hash_password);
  const decoded = JWT.decode(data.token)
  const creator = await Model.findOne({national_id:decoded.national_id })
  console.log("created_by:", decoded.national_id)

  

  const user = {
    first_name: data.first_name,
    last_name: data.last_name,
    address: data.address,
    national_id: data.national_id,
    phone_number: data.phone_number,
    password: hash_password,
    is_valid: true,
    birth_date: data.birth_date,
    email: data.email,
    created_by: creator._id,
    created_at: new Date(),
    is_deleted: false,
    deleted_by: null,
    deleted_at: null,
    updated_by: null,
    updated_at: null,
    position_id: data.position_id,
    permission: data.permission,
    unit_id: data.unit_id,
    token: token,
  };
  new Model(user)
    .save()
    .then((result) => {
      res.status(201).json({ is_success: true, data: result});
    })
    .catch((err) => {
      console.log(err);
      res.status(400).json({ message: err, is_success: false });
    });
    
  
};

module.exports = { UserList, CreateUser , SingleUser , UpdateUser};
