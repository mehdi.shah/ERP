const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Type = mongoose.SchemaTypes;

const Model = new Schema({
  first_name: {
    type: Type.String,
    required: true,
  },
  last_name: {
    type: Type.String,
    required: true,
  },
  address: {
    type: Type.String,
    required: true,
  },
  national_id: {
    type: Type.Number,
    required: true,
    unique: true,
  },
  phone_number: {
    type: Type.Number,
    required: true,
    unique: true,
  },
  password: {
    type: Type.String,
    required: true,
  },
  is_valid: {
    type: Type.Boolean,
    required: true,
  },
  birth_date: {
    type: Type.Date,
    default: null,
  },
  email: {
    type: Type.String,
    default: null,
    require: true,
    unique: true,
  },
  created_by: {
    type: Type.ObjectId,
    default: null,
  },
  created_at: {
    type: Type.Date,
    default: new Date(),
  },
  is_deleted: {
    type: Type.Boolean,
    default: false,
  },
  deleted_by: {
    type: Type.ObjectId,
    default: null,
  },
  deleted_at: {
    type: Type.Date,
    default: null,
  },
  updated_by: {
    type: Type.ObjectId,
    default: null,
  },
  updated_at: {
    type: Type.Date,
    default: null,
  },
  position_id: {
    type: Type.ObjectId,
    default: null,
  },
  permission: {
    type: Type.Array,
    default: [],
  },
  unit_id: {
    type: Type.ObjectId,
    default: null,
  },
  token: {
    type: Type.String,
    default: null,
  },
  verify_code: {
    type: Type.Number,
    default: null,
  },
});

// Compile model from schema
const UserModel = mongoose.model("User_Model", Model);

module.exports = UserModel;
