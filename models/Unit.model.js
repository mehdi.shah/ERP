const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Type = mongoose.SchemaTypes;

const Model = new Schema({
  name: {
    type: Type.String,
    default: null,
    required: true,
  },
  manager: {
    type: Type.ObjectId,
    require: true,
  },
  created_by: {
    type: Type.ObjectId,
    default: null,
  },
  created_at: {
    type: Type.Date,
    default: new Date(),
  },
  is_deleted: {
    type: Type.Boolean,
    default: false,
  },
  deleted_by: {
    type: Type.ObjectId,
    default: null,
  },
  deleted_at: {
    type: Type.Date,
    default: null,
  },
  updated_by: {
    type: Type.ObjectId,
    default: null,
  },
  updated_at: {
    type: Type.Date,
    default: new Date(),
  },
});

const UnitModel = mongoose.model("Unit_Model", Model);

module.exports = UnitModel;
