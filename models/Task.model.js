const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Type = mongoose.SchemaTypes;

const Model = new Schema({
  title: {
    type: Type.String,
    required: true,
  },
  assign_to_user: {
    type: Type.ObjectId,
    default: null,
  },
  assign_to_unit: {
    type: Type.ObjectId,
    default: null,
  },
  date: {
    type: Type.Date,
    default: new Date(),
  },
  due_date: {
    type: Type.Date,
    default: null,
  },
  short_description: {
    type: Type.String,
    default: null,
    require: true,
  },
  description: {
    type: Type.String,
    default: null,
  },
  created_by: {
    type: Type.ObjectId,
    default: null,
  },
  created_at: {
    type: Type.Date,
    default: new Date(),
  },
  is_deleted: {
    type: Type.Boolean,
    default: false,
  },
  deleted_by: {
    type: Type.ObjectId,
    default: null,
  },
  deleted_at: {
    type: Type.Date,
    default: null,
  },
  updated_by: {
    type: Type.ObjectId,
    default: null,
  },
  updated_at: {
    type: Type.Date,
    default: new Date(),
  },
  status: {
    type: Type.String,
    default: "In_Progress",
  },
  priority: {
    type: Type.String,
    default: "Medium",
  },
  project_id: {
    type: Type.ObjectId,
    default: null,
  },
  comment: {
    type: Type.String,
    default: null,
  },
});

const TaskModel = mongoose.model("Task_Model", Model);
module.exports = TaskModel;