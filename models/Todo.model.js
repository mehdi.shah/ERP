const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Type = mongoose.SchemaTypes;

const Model = new Schema({
  title: {
    type: Type.String,
    required: true,
  },
  assign_to: {
    type: Type.ObjectId,
    required: true,
    default: null,
  },
  date: {
    type: Type.Date,
    default: new Date(),
  },
  priority: {
    type: Type.String,
    default: "Medium",
  },
  task_id: {
    type: Type.ObjectId,
    default: null,
  },
  created_by: {
    type: Type.ObjectId,
    default: null,
  },
  created_at: {
    type: Type.Date,
    default: new Date(),
  },
  is_deleted: {
    type: Type.Boolean,
    default: false,
  },
  deleted_by: {
    type: Type.ObjectId,
    default: null,
  },
  deleted_at: {
    type: Type.Date,
    default: null,
  },
  updated_by: {
    type: Type.ObjectId,
    default: null,
  },
  updated_at: {
    type: Type.Date,
    default: new Date(),
  },
});

const TodoModel = mongoose.model("Todo_Model", Model);
module.exports = TodoModel;
