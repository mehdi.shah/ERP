const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Type = mongoose.SchemaTypes;

const Model = new Schema({
  about_project: {
    type: Type.String,
    required: true,
  },
  start_date: {
    type: Type.Date,
    default: new Date(),
    required: true,
  },
  end_date: {
    type: Type.Date,
    default: new Date(),
    required: true,
  },
  owner: {
    type: Type.ObjectId,
    default: null,
    required: true,
  },
  tags: {
    type: Type.Array,
    default: [],
  },
  budget: {
    type: Type.Number,
    default: 0,
  },
  manager: {
    type: Type.ObjectId,
    default: null,
  },
  members: {
    type: Type.Array,
    default: [],
  },
  created_by: {
    type: Type.ObjectId,
    default: null,
  },
  created_at: {
    type: Type.Date,
    default: new Date(),
  },
  is_deleted: {
    type: Type.Boolean,
    default: false,
  },
  deleted_by: {
    type: Type.ObjectId,
    default: null,
  },
  deleted_at: {
    type: Type.Date,
    default: null,
  },
  updated_by: {
    type: Type.ObjectId,
    default: null,
  },
  updated_at: {
    type: Type.Date,
    default: new Date(),
  },
  status: {
    type: Type.String,
    default: "Pending",
  },
});

const ProjectModel = mongoose.model("Project_Model", Model);
module.exports = ProjectModel;
